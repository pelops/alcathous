alcathous.algorithms package
============================

Submodules
----------

alcathous.algorithms.abstractalgorithm module
---------------------------------------------

.. automodule:: alcathous.algorithms.abstractalgorithm
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.algorithmfactory module
--------------------------------------------

.. automodule:: alcathous.algorithms.algorithmfactory
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.average module
-----------------------------------

.. automodule:: alcathous.algorithms.average
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.count module
---------------------------------

.. automodule:: alcathous.algorithms.count
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.maximum module
-----------------------------------

.. automodule:: alcathous.algorithms.maximum
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.minimum module
-----------------------------------

.. automodule:: alcathous.algorithms.minimum
    :members:
    :undoc-members:
    :show-inheritance:

alcathous.algorithms.weightedaverage module
-------------------------------------------

.. automodule:: alcathous.algorithms.weightedaverage
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: alcathous.algorithms
    :members:
    :undoc-members:
    :show-inheritance:
