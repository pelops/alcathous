.. alcathous documentation master file, created by
   sphinx-quickstart on Sun Nov 25 12:39:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to alcathous's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   alcathous
   setup
   tests_unit


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
