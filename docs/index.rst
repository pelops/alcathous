.. alcathous documentation master file, created by
   sphinx-quickstart on Sun Nov 25 14:01:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to alcathous's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   modules

.. mdinclude:: ../README.md

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
